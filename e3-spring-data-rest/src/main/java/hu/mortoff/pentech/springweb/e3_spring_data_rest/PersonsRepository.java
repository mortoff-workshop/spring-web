package hu.mortoff.pentech.springweb.e3_spring_data_rest;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonsRepository extends JpaRepository<Person, String> {
}
