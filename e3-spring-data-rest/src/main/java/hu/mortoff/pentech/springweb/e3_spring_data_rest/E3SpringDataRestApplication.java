package hu.mortoff.pentech.springweb.e3_spring_data_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class E3SpringDataRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(E3SpringDataRestApplication.class, args);
	}

}
