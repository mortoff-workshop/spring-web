[[_TOC_]]

Mortoff Műhely: Spring Web
==========================

### IDE és fejlesztői eszközök telepítése

#### Git

Elosztott verziókezelő rendszer. Mára nagyjából egyeduralkodó, érdemes megtanulni használni,
de ez nem elengedhetetlen a mai alkalomhoz. 

#### IDE: IntelliJ Idea

Community verzió letölthető a https://www.jetbrains.com/idea/download/ címről.

Pendrive-on körbeadom annak, akinek lassú lenne a letöltése.

Saját felelősségre lehet más IDE-t használni abban az esetben, ha valaki gyakorlott 
az adott eszközzel történő java fejlesztésben. Maven és JDK megléte önállóan biztosítandó ekkor.

#### JDK: VM + Java API

IntelliJ IDEA-n keresztül letölthető.

#### Maven: package manager

IntelliJ IDEA tartalmazza.

### Történet

#### 2000-es évek eleje

Az Internet már kezdi meghódítani a világot. Az emberek zsebében már ott a mobil, de
többnyire csak telefonálásra használják. A WAP, kihajthatós, billentyűzetes Nokia csúcsmodellek, PDA-k, 
BlackBerry-k használata csupán szórványos.   

A Microsoft Office szinte minden irodában adott. A drága és lassú ISDN-t kezdi leváltani az ADSL.
Online közösségi média nem létezik, de a weblapokra már Google keresővel lehet rátalálni és sokaknak privát
e-mail címe is van. Ekkor jelenik meg majd terjed el a Firefox böngésző. A Chrome-ra még pár évet várni kell.

A vállalatok többsége az Excel-en kívül valamilyen offline ügyviteli rendszert használ. A floppy diskek már ekkor
elavultak, de az adatátvitel, jelentési kötelezettségek egyik legegyszerűbb formája. A e-mail használatától
sokan vonakodnak hivatalos iratok esetén. A posta és fax is meghatározó még az üzleti dokumentumok továbbításához.
A LAN ekkor még meghatározóbb a digitális adatátvitelben, mint az Internet, így digitálisan általában az inteézményen
belüli nyilvántartási adatok közlekednek, nem ritkán megosztott mappák formájában.

A C++ programozási nyelv egyre nagyobb népszerűségnek örvend. Jelentősek még a Borland Delphi fejlesztések.
Elterjendnek a grafikus felhasználói felületek gyors tervezésére, kialakítására használható eszközök (RAD).
Alapvető követelménnyé válik, hogy a felhasználó felülethez relációs adatbázisból származó adatok legyenek
köthetők. Ezzel hódit a Borland Delphi és a Microsoft Visual termékcsaládja is. Kommunikáció: CORBA.

A PHP él és virul. Hírportálok, blogok előszeretettel alkalmazzák dinamikus webes tartalom előállításához.
A vállalatok kezdenek ráugrani a webes technológiákra (.NET lufi), de még nicsenek azzal tisztában, hogy
pontosan milyen üzleti lehetőségeik vannak. Média, reklám, CMS, ami működik online, minden más még csak 
kísérleti stádiumban van.

A Java nyelv létezik, de viszonylag új, kevesen ismerik. Összességében a konkurens cégek RAD eszközeivel
könnyebb és gyorsabb fejelszteni. A Microsoft a piaci pozíciójából adódóan jobban el tudja, az egyébként
fejlettebb megoldásait. 

#### 2000-es évek második fele

Az _XML_ alapú adattárolás és kommunikáció népszerűsége megnő. Megjelennek a SOAP alapú webszolgáltatások.
Egyre nagyobb szerepet kap a szabványosítás, formalizálás, modellezés. MVC mind desktopon mind weben 
népszerű megközelítés.

A korszerű fejlesztőeszközök ebben az időben támogatták egy grafikus felhasználói felületen létrehozott,
olyan _XML_ fájlba mentett modell létrehozását, amiből az eszköz forráskódot generál. Az XML alapú modell
általában valamilyen szabványos magasabb szintű formátumban készült, vagy a fejlesztő eszköz szabványos
jelöléseket tartalmazó formátumokat volt képes XML-be transzformálni (pl. _UML_, _BPMN_, _ER_ digrammok).

A már elterjedt, népszerű adatbáziskezelő rendszereket (_MS SQL Server_, _Oracle_, _MySQL_, _PostgreSQL_) 
felruházták ún. félig sturktúrált, XML adattárolási lehetőséggel, valamint a hagyományos táblák adatainak
_XML_ formátumba történő lékérdezését, transzformálását.

A java nyelv kezd elterjedni, elsősorban üzleti, webes környezetben. A nagyobb vállalatok az üzletvilelben
is egyre inkább online megoldásokra térnek át, alkalmazás szervereket telepítenek. Hódít a SOA. 
Az ORM megoldásokat és a 3 rétegű webes architektúrát előszeretettel alkalmazzák.

A közösségi oldalak, alkalmazások még gyerekcipőben járnak, kiaknázatlanok. A _Facebook_ már létezik, de nem 
sokan ismerik, ellenben az _iWiW_-et igen. Sok fiatal "csetel", a _Windows Live (MSN) Messenger_ a legnépszerűbb.
Az üzeltben az azonnali üzenetváltás és a konferenciabeszélgetés még nem annyira népszerű.

Agilis módszertanok: bleading edge, főként külföldön és akadémiai körökben.

#### 2010-es évek első fele

A _Google_ cég eddigre még meghatározóbbá vált, megjelent a _Chrome_ böngésző, a Maps, az _Android_. 
Még az elején a _Gmail_ meghívásos alapon működött, eddigre már bárki szabadon regisztrálhatott _Gmail_ címet.
A közösségi média is rohamosan terjedt. Rengetegen regisztráltak _Facebook_ fiókot.
A magán és üzleti életben is megjelentek a konferenciabeszélgetések a _Skype_ révén.

A közéletben is megjelent az online ügyintézés, de a vállalatok is előszeretettel alkalmaznak online 
ügyviteli rendszereket. A nagyobb vállalatoknál jelentős digitális transzformáció indult.
Elkedztek terjedni az érintésmentes kártyák és egyéb RFID technológiák.

Megjelentek az online fizetési megoldások, online közműszámlák.
Tömegesen indulnak be az adatgyűjtő, automatizációs, digitalizációs projektek. _Industrie 4.0_

A java web alkalmazás fejlesztés a virágkorát éli. Az új online fejlesztések többnyire java alapúak. 
Meghatározó webes körökben a _Ruby on Rails_ is. A PHP kissé háttérbe szorul.

Egyes vállalatok még desktop java alkalmazásokat és appletetket is fejlesztetnek és használnak, de ezen a téren
továbbra is erősebb az új _Microsoft Visual Studio .NET_ termékcsalád és az _Adobe Flash_.

Megjelennek a frontend webalkalmazások a client-side rendering koncepciójával a server-side rendering megoldások mellett.
Az _Adobe Flash_-es webkliensek népszerűek, de a _JavaScript_ egyre erősebb.

Az _XML_ szerepét lassan átveszi a _JSON_ és a _YML_, a _SOAP_ szerepét a _REST_.

Megjelenik a cloud és a konténerizáció. Az üzemeltetésben széleskörűen felismerik a virtualizáció előnyeit.

Agilis módszertanok: haladó vállalkozások, hazai kkv-ban is.

#### 2010-es évek második fele

Tovább terjeszkedik a fintech ipar. Megjelennek a kriptovaluták (a köztudatban is).

A _mobile first_ szemlélet és az _AJAX_ technológiák használata, és a _frontend_/_backend_ szeparáció a webes 
világban alapelvárássá válik.

A natív mobilfejlesztés egyelőre még túlsúlyban van a web alapú megoldásokkal szemben. 

A _JavaScript_ győzedelmeskedik a _Flash_ felett. A korábbi riválisok (Silverlight, Air, Flex) is megszűnnek, de
megjelenik a _TypeScript_. _Angular_ és _React_ keretrendszerek válnak a legnépeszerűbbekké. 

A _Python_ átveszi a _Ruby_ és az _R_ szerepét.

A _Go_ teret hódít, mint backend nyelv.

Az IT világ _Big Data_ és _microservices_ lázban ég. 

Kezdenek széleskörűen elterjedni az mesterséges intelligencia alkalmazások, chat robotok, ipari automatizálás, világszerte.
Egyre több a digitalizációs projekt.

A felhőalapú és konténerizált megoldások széleskörűen elterjednek.

Agilis módszertanok: a multiknál is széleskörűen alkalmazzák, általános a fejlesztésben.

#### A jelen

A _TypeScript_ a legleterjedtebb frontend nyelv. A natív desktop fejlesztés erősen háttérbe szorul, web alapú,
általában _TypeScript_ nyelven írt alkalmazások terjednek desktop és mobil platformokon is. A frontend fejlesztői
pozíciók népszerűbbé válnak, a fronted fejlesztői szakma elfogadottabbá és keresettebbé válik.

A webes frontend és backend fejlesztés az, ami igazából a szoftverfejlesztés nagy részét adja.
Jelentős a beágyazott és célhardveres (játékfejlesztés is ide sorolható) fejlesztés szerepe is.

Ezeken kívül említésre méltók a _low code_/_no code_ megoldások, a _data engineering_, 
a mesterséges intelligencia és automatizálási megoldások is, de sok esetben jellemzően ezekhez valamilyen
webes megoldás is kapcsolódik, pl. media streaming, adatgyűjtés, az automatizált megoldás webes rendszert
automatizál, az elemzett adatok végül webes felhasználói felületen jelennek meg.

Agilis módszertanok: már a lemaradók/fontolva haladók is bevezetik.

### Mire jók a RESTful webszolgáltatások?

A REST API-k által nyújtott webszolgáltatásokat RESTful webszolgáltatásoknak nevezzük.

A REST API-k lényege, hogy erőforrásnak (resource) nevezett entitásokon végzett műveletek segítségével
valósítják meg a komponensek közti kommunikációt. A műveletek kialakítására ajánlások vannak, kvázi szabványosak.

A műveletek illeszkednek a HTTP protokollhoz, egy a lekérés, létrehozás, módosítás, törlés rendszerint a 
GET, POST, PUT/PATCH, DELETE kérés típusokkal valósítható meg. Itt a GET kérés az, amit a böngészős használatnál
rendszerint az oldalak és egyéb média elemek lekérésére használ a böngésző, a POST pedig, ami egy űrlap (form) 
elküldésekor hívódik.

A rendszerünk, amit fejlesztünk különböző, egyenként fejelszthető, tesztelhető, telepíthető szolgáltatásokból
áll össze. Az egyes szolgáltatásokat, pl. marketing e-mailek kilüldését, számlázást akár az általunk fejlesztett
rendszeren kívüli szolgáltatásokként vehetjük igénybe, harmadik féltől.

A szolgáltatások elosztott módon való kezelése azt is lehetővé teszi, hogy egy harmadik féltől származó
szolgáltatást idővel a saját rendszerünk belső szolgáltatásával helyettesítsünk, vagy épp fordítva, egy
saját szolgáltatást egy fejlettebb, kiszervezett szolgáltatással helyettesítsünk.

A REST szolgáltatások gépek közötti kommunikációt valósítanak meg. Ha ügyfeleinknek egy olyan integrált rendszert
biztosítunk, ahol az egyes szolgáltatások kielégítéséhez más vállalatok szolgáltatásait is igénybe vesszük,
akkor az igénybevett szolgáltatásokat b2b (Business-to-Business) szolgáltatásoknak nevezzük. Az ügyfeleinknek
nyújtott szolgáltatások pedig a b2c (Business-to-Customer) szolgáltatások.

Az ügyfeleinknek REST API elérést is biztosíthatunk, mint szolgáltatást. Ekkor az ügyfelek rendszerét az
ügyfelek fejlesztőinek kell integrálniuk a miénkhez. Például a Spotify és a Netflix egyes tartalmat
a partnereiktől szerik be, akár valós időben, amikor az ügyfeleik a zenéket hallgatni vagy a videókat nézni kezdik.
Hasonlóan, mikor az ügyfelek online számlát kapnak vagy fizetnek egy weboldalon vagy mobil appban, ott is
egy harmadik fél számlázási vagy fizetési API-ját veszik igénybe az ügyfelek. 

A modern frontend (felhasználói felület, web kliens) és backend (szerver oldal) részeket tartalmazó webalkalmazások esetében a
backend részt REST API és Websockets használatával valósítják meg.

Egyre több asztali alkalmazást váltanak le webalkalmazással. Napjainkban gyakori, hogy egy webklienst csomagolnak
be és terjesztenek asztali vagy mobil alkalmazásként. Ilyen például a Microsoft Teams vagy a Visual Studio Code is.
Az online webalkalamzások előnye, hogy nem a végfelhasználónak kell őket karbantartani (frissíteni), telepítésük
(ha kell egyáltalán telepíteni) egyszerűbb. A kikényszerített frissítések által nem kell számolni különböző kliens
verziókkal, inkompatibilitásokkal. A webes rendszerek közel platformfüggetlenek.

### Technikai érdekességek

#### Java appletek és JavaScript

A java applet egy korai, elavult java technológia. Segítségével a webböngészők grafikus felülettel rendelkező
java alkalmazásokat tudtak indítani és megjeleníteni a weboldalon belül, ill az appletek saját ablakokat is nyithattak
a böngészőablakon kívül. Az Adobe Flash megjelenése és elterjedése háttérbe szorította a java appleteket.
A java applet kliens oldali technológia, futtatásához nincs szükség speciális kiszolgálóra. A java fejlesztők,
akik kevés kivétellel kizárólag backend fejlesztők, általában érzékenyek arra, ha applet vagy asztali alkalmazás
fejlesztőnek (pl. ÁNYK, abevjava) fejlesztőnek gondolják őket, vagy a java technológiákat ezekkel azonosítják.

A JavaScript a java appletekhez hasonlóan, alapvetően kliens oldali technológia. A böngészőprogramok képesek JavaScript
kódot futtatni. Gyakorlatilag közel az összes web frontend alkalmazás futó kódja JavaScript kód. A TypeScript és
egyéb frontend nyelvek (pl. Dart) is JavaScript kódra fordul. A böngészők JavaScript kódon kívül csak WebAssembly-t
képesek futtatni, az utóbbi lehetőség azonban csak pár éve elérhető és nem terjedt még el széles körben. Fontos
megemlíteni, hogy a JavaScript csak a nevében és a C alapú szintaktikában hasolít a Java programozási nyelvre.
Más közük nincs egymáshoz.

A JavaScript a Node.js keretrendszernek köszönhetően szerver oldalon, backend fejlesztére is használható, viszont
ez kevésbé jellemző, mint a kliens oldali, azaz fronted felhasználása.

#### A java webalkalmazások, PHP szkriptek összehasonlítása

Ha szerver oldali webes technológiákról beszélünk, a javát és a PHP-t is meg kell említenünk, ám a kettő alapvetően
eltér egymástól. Nem csak a programozási nyelvek szintaktikájában és típusosságában vannak eltérések, hanem
a programok futási módjában is, és igazából ebből adódnak fő előnyeik, hátrányaik és felhasználási területeik
a webes világban.

Egy java webalkalmazás a szerver (pl. servlet container) indításakor betöltődik és fut egy Java virtuális gépen.
Egy alkalmazás indítás után tehát több HTTP kérés kiszolgálható. A alkalmazás erőforrásain (pl. adatbázis kapcsolatok),
gyorsítótár a különböző kérések kiszolgálásakor osztozhat az alkalmazás. Ebből adódóan az alkamazás telepítése és
indítása több másodpercet is igénybe vehet, azonban a kérések kiszolgálása gyors. Az alkalmazás forráskódja a
kiszolgálóra való telepítés előtt fordítást igényel. Ennek előnye viszont, hogy fordítás során már sok hiba
kiderülhet, annál is inkább, hogy a java erősen típusos objektum orientált nyelv, szigorú láthatósági szabályokkal.

Egy PHP webalkalmazásnál egy PHP szkript fájl és az általa hivatkozott fájlok egy bejövő HTTP kérés hatására
fordítódnak le és hajtódnak végre. A kérés kiszolgálása után az alkalmazás futása végetér. Az alkalmazás
nem igényel különösebb telepítést a php fájlok másolásán kívül, indítása gyors. Futtatása általában kissé
lassabb, mint a java alkalmazásoké. Cserében a fejlesztést gyorsítja, hogy a webkiszolgálón szerkesztett PHP
forrásfájlok módosításának hatására a változtatások azonnal látszanak a következő bejövő kérés kiszolgálásakor,
ellenben egy java alkalmazás esetén rendszerint a projekt újrafordítása, csomagolása, telepítése és a szerver
újraindítása szükséges.

#### SOAP és REST webszolgáltatások

A SOAP XML alapú, HTTP fölött is alkalmazható kommunikációs protokoll, amivel távoli eljáráshívás (RPC) valósítható
meg. Az támogatott hívások és az üzenetek formátuma WSDL leíróban és XML sémában (XSD) rögzítettek, validálhatók.
A 2000-es évek második felének meghatározó képi kommunikációs megoldása. A CORBÁ-t és az XML-RPC-t váltotta le.

A 2010-es években a SOAP/WSDL/XML technológiákat fokozatosan leváltották a REST/OpenAPI/JSON technológiák.
Ezek előnyei a kisebb adatforgalom, kevesebb, szabványosabb művelet (CRUD, HTTP verbs/request methods),
ember által könnyebben olvasható és megérthető.
Számos előnyük mellett hátrányuk, hogy JSON objektumok validálására bár létezik pár megoldás, azok nem annyira
fejlettek, szabványosak, kifejezők és egységesek, mint XML esetén az XSD.

#### Java web alkalmazások app szerveren és azon kívül

A java webalkalmazások általában egy servlet container nevű speciális webes szerver alkalmazáson belül futnak.
Ebben az esetben a Java Servlet API írja le, hogy a bejövő HTTP kéréseket az alkalmazás hogyan szolgálja ki,
milyen képességekkel kell rendelkeznie a servlet containereknek.

Az alkalmazás kiszolgálók vagy app(lication) serverek az egyszerű servlet containerek mellett képesek az
alkalmazások mellett egyéb erőforrások kezelésére is. Így név szerint elérhető adatbázis kapcsolatok,
üzenetsorok (message queue), konfigurálhatók felületeiken, amiken a telepített alkalmazások osztoznak.
Ezen kívül szabványos JavaEE függvénykönyvtárakat is elérhetővé tesznek, amik megkönnyíthetik az alkalmazás
fejlesztést. Ebben az esetben az alkalmazás szerverk kiválasztása határozza meg, hogy hogyan, milyen
eszközökkel, könyvtárakkal fejleszthetünk alkalmazást.

A Spring keretrendszer ezzel szemben sok, egymással kompatíbilis függvénykönyvtárat kínál, melyek többé-kevésbé
egyenértékűek a JavaEE rendszerrel, néhol kiterjesztve vagy egyszerűsítve azt. A szükséges library-k az
alkalmazás webarchívumába kerülnek fordításkor (build). A webarchívumok tetszőleges servlet container-en
elhelyezhetők.

A Spring Boot a Spring keretrendszeren felül értelmes alapbeállításokat (sensible defaults) tartalmaz a
Ruby on Rails mintájára, egymással jól összefüggő komponens csomagokat definiál és lehetővé teszi a servlet
container alkamazásba való becsomagolását. Így egy alkalmazás servlet containerrel együtt történő futtatásához
csak egy java virtuális gépre (a JDK, azaz Java Development Kit része) van szükség.

### Kezdeti lépések

#### Spring Web projekt létrehozása

1. Alap projekt letöltése a szükséges beállításokkal, könyvtárakkal
   
   * Innen tölthető le: https://start.spring.io/
   * GroupId: `hu.mortoff.pentech.springweb`
   * ArtifactId: `e1-spring-start`
   * Package: nem tartalmazhat kötőjelet, átírandó aláhúzásjelre, vagy törlendő
   * Dependencies: 
     * Spring Web
     * Lombok
     * Spring Boot Actuator 

2. Projekt megnyitása az IDE-ben
   * Kicsomagolás egy szimpatikus helyre
   * A fejlesztői környezetben Open...
   * `pom.xml` (vagy mappa) kitallózása

3. Forrás fájlok és konfiguráció ellenőrzése

#### Futtatás 

Futtatás IDE-ből.

#### Monitorozás 

http://localhost:8080/actuator/health/

#### Deployment

```shell
mvnw clean install
java -jar target/e2-spring-json-0.0.1-SNAPSHOT.jar
```

### REST végpont JSON válasszal. Jackson és Lombok

1. Alap projekt letöltése a szükséges beállításokkal, könyvtárakkal
    * Innen tölthető le: https://start.spring.io/
    * GroupId: `hu.mortoff.pentech.springweb`
    * ArtifactId: `e2-spring-json`
    * Package: nem tartalmazhat kötőjelet, átírandó aláhúzásjelre, vagy törlendő
    * Dependencies:
        * Spring Web
        * Lombok
        * Spring Boot Actuator

2. Projekt hozzáadása az IDE-ben
   * Letöltés és kicsomagolás
   * Maven fül jobb oldalt, + gomb, `pom.xml` tallózása

3. Forráskód kiegészítése
   * `Person` DTO osztály létrehozása
     * `@Data` lombok annotáció alkalmazása (konstruktor, getter, setter, equals, hashCode, toString)
     * `@Value` annotáció is lehet `@Data` helyett: később lesz róla szó, bonyolultabb.
   * `PersonController` osztály 
     * `@RestController`, `@RequestMapping` és `@GetMapping` annotációk alkalmazása

```java
@Data
public class Person {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String placeOfBirth;

}
```

```java
@RestController
@RequestMapping("people")
public class PersonController {

    private Person jakab;
    private Person geza;
    private Person boglarka;
    private List<Person> people;

    public PersonController() {
        jakab = new Person();
        jakab.setFirstName("Jakab");
        jakab.setLastName("Gipsz");
        jakab.setDateOfBirth(LocalDate.of(2001, 3, 2));
        jakab.setPlaceOfBirth("Mosonmagyaróvár");

        geza = new Person();
        geza.setFirstName("Geza");
        geza.setLastName("Kekazeg");
        geza.setDateOfBirth(LocalDate.of(1998, 5, 18));
        geza.setPlaceOfBirth("Budapest");

        boglarka = new Person();
        boglarka.setFirstName("Boglárka");
        boglarka.setLastName("Réti");
        boglarka.setDateOfBirth(LocalDate.of(1998, 5, 18));
        boglarka.setPlaceOfBirth("Debrecen");

        people = List.of(jakab, geza, boglarka);
    }

    @GetMapping
    public List<Person> findAll() {
        return people;
    }

}
```

4. http://localhost:8080/people link alatt megtaláljuk az új REST végpontunkat.

### Low-code megoldás Spring Data REST-tel

Az előző példához relációs in-memory adatbázist adunk hozzá, ami bármikor lecserélhető
MS SQL Server, Oracle, MySQL vagy PostgreSQL adatbázisok valamelyikére, kódmódosítás nélkül.

1. Alap projekt letöltése a szükséges beállításokkal, könyvtárakkal
    * Innen tölthető le: https://start.spring.io/
    * GroupId: `hu.mortoff.pentech.springweb`
    * ArtifactId: `e3-spring-data-rest`
    * Package: nem tartalmazhat kötőjelet, átírandó aláhúzásjelre, vagy törlendő
    * Dependencies:
        * Spring Web
        * Lombok
        * Spring Boot Actuator
        * Spring Data JPA
        * Rest Repositories
        * H2 Database

2. Projekt hozzáadása az IDE-ben
    * Letöltés és kicsomagolás
    * Maven fül jobb oldalt, + gomb, `pom.xml` tallózása

3. Forráskód kiegészítése
   * `Person` DTO osztály létrehozása
       * `@Getter`, `@Setter` lombok annotáció alkalmazása
       * `@Entity`, `@Id` JPA annotációk alkalmazása
   * `PersonsRepository` osztály létrehozása
   * `src/main/resuorces/data.sql`-ban indításkori adatbetöltő SQL utasításokat adhatunk meg.
   * `src/main/resuorces/application.properties`-ben be kell állítani a JPA-nak, hogy akkor is generáljon táblákat, 
      az adatbázisban, ha van van iniciaalizáló `data.sql` fájlunk: `spring.jpa.defer-datasource-initialization=true`

```java
@Getter
@Setter
@Entity
public class Person {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String placeOfBirth;

}
```

```java
public interface PersonsRepository extends JpaRepository<Person, String> {
}
```

src/main/resources/application.properties:
```properties
spring.jpa.defer-datasource-initialization=true
```

src/main/resources/data.sql:
```sql
insert into person (id, first_name, last_name, date_of_birth, place_of_birth)
values (1, 'Jakab', 'Gipsz', '2001-03-02', 'Mosonmagyaróvár');

insert into person (id, first_name, last_name, date_of_birth, place_of_birth)
values (2, 'Geza', 'Kekazeg', '1995-05-18', 'Budapest');

insert into person (id, first_name, last_name, date_of_birth, place_of_birth)
values (3, 'Boglárka', 'Réti', '1998-08-21', 'Debrecen');
```

4. http://localhost:8080 link alatt megtaláljuk, hogy milyen REST végpontot hívhatunk.
    * Postman vagy valamilyen REST kliens ajánlott a teszteléshez.

### Más REST szolgáltatások meghívása

1. Alap projekt letöltése a szükséges beállításokkal, könyvtárakkal
    * Innen tölthető le: https://start.spring.io/
    * GroupId: `hu.mortoff.pentech.springweb`
    * ArtifactId: `e4-rest-template`
    * Package: nem tartalmazhat kötőjelet, átírandó aláhúzásjelre, vagy törlendő
    * Dependencies:
        * Spring Web
        * Lombok
        * Spring Boot Actuator
        * Spring Data JPA
        * H2 Database
