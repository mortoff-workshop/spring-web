package hu.mortoff.pentech.springweb.e2_spring_json;

import lombok.Data;
import java.time.LocalDate;

@Data
public class Person {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String placeOfBirth;

}
