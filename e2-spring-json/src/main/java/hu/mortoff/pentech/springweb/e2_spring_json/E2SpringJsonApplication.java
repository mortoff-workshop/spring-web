package hu.mortoff.pentech.springweb.e2_spring_json;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class E2SpringJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(E2SpringJsonApplication.class, args);
	}

}
