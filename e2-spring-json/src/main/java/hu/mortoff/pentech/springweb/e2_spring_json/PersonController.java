package hu.mortoff.pentech.springweb.e2_spring_json;

import java.time.LocalDate;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("people")
public class PersonController {

    private final List<Person> people;

    public PersonController() {
        Person jakab = new Person();
        jakab.setFirstName("Jakab");
        jakab.setLastName("Gipsz");
        jakab.setDateOfBirth(LocalDate.of(2001, 3, 2));
        jakab.setPlaceOfBirth("Mosonmagyaróvár");

        Person geza = new Person();
        geza.setFirstName("Geza");
        geza.setLastName("Kekazeg");
        geza.setDateOfBirth(LocalDate.of(1998, 5, 18));
        geza.setPlaceOfBirth("Budapest");

        Person boglarka = new Person();
        boglarka.setFirstName("Boglárka");
        boglarka.setLastName("Réti");
        boglarka.setDateOfBirth(LocalDate.of(1998, 5, 18));
        boglarka.setPlaceOfBirth("Debrecen");

        people = List.of(jakab, geza, boglarka);
    }

    @GetMapping
    public List<Person> findAll() {
        return people;
    }

}
