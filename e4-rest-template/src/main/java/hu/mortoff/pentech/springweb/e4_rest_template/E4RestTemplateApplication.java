package hu.mortoff.pentech.springweb.e4_rest_template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class E4RestTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(E4RestTemplateApplication.class, args);
	}

}
