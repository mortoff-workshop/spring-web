package hu.mortoff.pentech.springweb.e4_rest_template;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonsRepository extends JpaRepository<PersonEntity, String> {
}
