package hu.mortoff.pentech.springweb.e4_rest_template;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value
@Builder(setterPrefix = "with", toBuilder = true)
@JsonDeserialize(builder = Person.PersonBuilder.class)
public class Person {

    String firstName;
    String lastName;
    LocalDate dateOfBirth;
    String placeOfBirth;
    List<String> nameDays;

}
