package hu.mortoff.pentech.springweb.e4_rest_template;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonsRepository personsRepository;
    private final RestTemplate restTemplate = new RestTemplate();
    private final ObjectMapper objectMapper;

    public List<Person> findAll() {

        List<PersonEntity> entities = personsRepository.findAll();

        return entities.stream().map(entity -> Person.builder()
                .withFirstName(entity.getFirstName())
                .withLastName(entity.getLastName())
                .withDateOfBirth(entity.getDateOfBirth())
                .withPlaceOfBirth(entity.getPlaceOfBirth())
                .withNameDays(getNameDays(entity.getFirstName()))
                .build())
                .toList();
    }

    @SneakyThrows
    private List<String> getNameDays(String firstName) {
        String response = restTemplate.exchange(RequestEntity
                        .get("https://api.nevnapok.eu/nev/{name}", firstName)
                        .accept(MediaType.APPLICATION_JSON)
                        .build(), String.class).getBody();

        return objectMapper.readValue(response, new TypeReference<Map<String, List<String>>>() {}).get(firstName);
    }
}
