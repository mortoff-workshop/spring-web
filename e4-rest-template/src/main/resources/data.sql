insert into person (id, first_name, last_name, date_of_birth, place_of_birth)
values (1, 'Jakab', 'Gipsz', '2001-03-02', 'Mosonmagyaróvár');

insert into person (id, first_name, last_name, date_of_birth, place_of_birth)
values (2, 'Géza', 'Kékazég', '1995-05-18', 'Budapest');

insert into person (id, first_name, last_name, date_of_birth, place_of_birth)
values (3, 'Boglárka', 'Réti', '1998-08-21', 'Debrecen');
