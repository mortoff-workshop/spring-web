package hu.mortoff.pentech.springweb.e1_spring_start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class E1SpringStartApplication {

	public static void main(String[] args) {
		SpringApplication.run(E1SpringStartApplication.class, args);
	}

}
